# go-cdk-gitlab

This project is meant as an example to help folks get started with the gitlab terraform provider using the terraform CDK. Some resource to help:

* [How to install the terraform CDK](https://developer.hashicorp.com/terraform/tutorials/cdktf/cdktf-install)
* [The compiled gitlab CDK bindings](https://github.com/cdktf/cdktf-provider-gitlab-go)
