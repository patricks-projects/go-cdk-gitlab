module cdk.tf/go/stack

go 1.19

require github.com/aws/constructs-go/constructs/v10 v10.1.306

require github.com/hashicorp/terraform-cdk-go/cdktf v0.15.5

require (
	github.com/Masterminds/semver/v3 v3.2.0 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/yuin/goldmark v1.4.13 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/mod v0.9.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/tools v0.7.0 // indirect
)

require (
	github.com/aws/jsii-runtime-go v1.80.0
	github.com/cdktf/cdktf-provider-gitlab-go/gitlab/v6 v6.0.2
)
