package main

import (
	"strconv"

	"github.com/aws/constructs-go/constructs/v10"
	"github.com/aws/jsii-runtime-go"
	"github.com/cdktf/cdktf-provider-gitlab-go/gitlab/v6/group"
	"github.com/cdktf/cdktf-provider-gitlab-go/gitlab/v6/project"
	"github.com/cdktf/cdktf-provider-gitlab-go/gitlab/v6/provider"
	"github.com/hashicorp/terraform-cdk-go/cdktf"
)

func NewMyStack(scope constructs.Construct, id string) cdktf.TerraformStack {
	stack := cdktf.NewTerraformStack(scope, &id)

	// Create a top level group in GitLab
	provider.NewGitlabProvider(stack, jsii.String("provider"), &provider.GitlabProviderConfig{
		Token:   jsii.String("ACCTEST1234567890123"),
		BaseUrl: jsii.String("http://127.0.0.1:8080"),
	})

	ourGroup := group.NewGroup(stack, jsii.String("my-top-level-group"), &group.GroupConfig{
		Name: jsii.String("My Top Level Group"),
		Path: jsii.String("my-top-level-group"),
	})

	// Get the Id of our group (we're just discarding the error for example purposes)
	topLevelGroupID, _ := strconv.ParseFloat(*ourGroup.Id(), 32)

	project.NewProject(stack, jsii.String("my-new-project"), &project.ProjectConfig{
		Name:            jsii.String("My new project in my top level group"),
		Description:     jsii.String("This is a top level project created within a group"),
		VisibilityLevel: jsii.String("public"),

		// create our project into the group we created above
		NamespaceId: &topLevelGroupID,
	})

	return stack
}

func main() {
	app := cdktf.NewApp(nil)

	NewMyStack(app, "example-go-cdk")

	app.Synth()
}
